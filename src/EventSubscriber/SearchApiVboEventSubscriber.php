<?php

namespace Drupal\search_api_vbo\EventSubscriber;

use Drupal\views_bulk_operations\EventSubscriber\ViewsBulkOperationsEventSubscriber;
use Drupal\views_bulk_operations\ViewsBulkOperationsEvent;
use Drupal\views\ResultRow;
use Drupal\Core\Entity\EntityInterface;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;

/**
 * Defines module event subscriber class.
 *
 * Allows getting data of core entity views.
 */
class SearchApiVboEventSubscriber extends ViewsBulkOperationsEventSubscriber {

  // Subscribe to the VBO event with lower priority.
  const PRIORITY = 0;

  /**
   * {@inheritdoc}
   */
  public function provideViewData(ViewsBulkOperationsEvent $event) {
    if ($event->getProvider() === 'search_api') {
      $base_table = $event->getView()->storage->get('base_table');
      $index = SearchApiQuery::getIndexFromTable($base_table);
      if ($index) {
        $entity_types = [];
        foreach ($index->getDatasources() as $datasource) {
          $entity_types[] = $datasource->getEntityTypeId();
        }
        $entity_types = array_unique(array_filter($entity_types));
        $event->setEntityTypeIds($entity_types);
        $event->setEntityGetter([
          'file' => __FILE__,
          'callable' => '\Drupal\search_api_vbo\EventSubscriber\SearchApiVboEventSubscriber::getEntityFromRow',
        ]);
      }
    }
  }

  /**
   * Get entity from views row.
   *
   * @param \Drupal\views\ResultRow $row
   *   Views result row.
   * @param string $relationship_id
   *   Id of the view relationship.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   The translated entity.
   */
  public static function getEntityFromRow(ResultRow $row, $relationship_id) {
    // TODO: Take relationship into account.
    //   @see \Drupal\search_api\Plugin\views\field\SearchApiFieldTrait::getCombinedPropertyPath()
    //   @see \Drupal\search_api\Plugin\views\field\SearchApiFieldTrait::getEntity()

    $object = $row->_object ?: $row->_item->getOriginalObject();
    $entity = $object->getValue();
    if (!($entity instanceof EntityInterface)) {
      return NULL;
    }

    // TODO: I think this is not necessary. The object stored in the row/item
    //   should already be translated, since Search API items always have a
    //   fixed language. - dm
//    if ($entity->isTranslatable()) {
//      // May not always be reliable.
//      $language_field = $entity->getEntityTypeId() . '_field_data_langcode';
//      if ($entity instanceof TranslatableInterface && isset($row->{$language_field})) {
//        return $entity->getTranslation($row->{$language_field});
//      }
//    }
    return $entity;
  }

}
